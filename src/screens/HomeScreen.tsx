import React, { ChangeEvent, FormEvent, useState } from "react";
import { useHistory } from "react-router-dom";
import Layout from "../components/Layout";

export default function HomeScreen() {
  const [username, setUsername] = useState<string>("");
  const { push } = useHistory();

  const onInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    setUsername(e.target.value);
  };

  const onSubmitPress = async (e: FormEvent) => {
    e.preventDefault();

    push(`/welcome/${username}`);

    if (!username) {
      alert("Please provide a name");
      return;
    }
  };

  return (
    <Layout>
      <>
        <p>Hi! How should I call you?</p>
        <form onSubmit={onSubmitPress}>
          <input
            type="text"
            onChange={onInputChange}
            placeholder="Name"
            name="name"
            style={{
              width: "300px",
              padding: "20px",
            }}
          />
        </form>
      </>
    </Layout>
  );
}
