import React, { useEffect, useState } from "react";
import logo from "../logo.svg";
import moment from "moment";
import { useParams } from "react-router-dom";
import getJsonData from "../utils/getJsonData";
import Layout from "../components/Layout";

// This API Key should ideally be placed under .env but in the interest of time, I've added it here.
const TIMEZONEDB_API_KEY = `44LJVHY2RGPC`;
const MANILA_ENDPOINT = `https://api.timezonedb.com/v2.1/get-time-zone?key=${TIMEZONEDB_API_KEY}&by=zone&zone=Asia/Manila&format=json`;
const MELBOURNE_ENDPOINT = `https://api.timezonedb.com/v2.1/get-time-zone?key=${TIMEZONEDB_API_KEY}&by=zone&zone=Australia/Melbourne&format=json`;
const TIME_FORMAT = "h:mm a";

export default function WelcomeScreen() {
  const [manilaTime, setManilaTime] = useState<string>("");
  const [melbourneTime, setMelbourneTime] = useState<string>("");
  const [error, setError] = useState<string>();
  const { username } = useParams<{ username: string }>();
  const [isLoading, setIsLoading] = useState<boolean>(false);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      Promise.all([
        getJsonData(MANILA_ENDPOINT),
        getJsonData(MELBOURNE_ENDPOINT),
      ])
        .then((data: any) => {
          const [manilaData, melbourneData] = data;

          if (Boolean(manilaData) && Boolean(melbourneData)) {
            const manilaTime = moment(manilaData!.formatted).format(
              TIME_FORMAT
            );
            const melbourneTime = moment(melbourneData!.formatted).format(
              TIME_FORMAT
            );
            setManilaTime(manilaTime);
            setMelbourneTime(melbourneTime);
          }

          setIsLoading(false);
        })
        .catch((e) => {
          setError(
            "We're currently using a free account to access the TimezoneDB API. It seems that we have exceeded the rate limit. Please try again later!"
          );
          setIsLoading(false);
        });
    };

    fetchData();
  }, []);

  let body = <p>Loading...</p>;

  if (!isLoading) {
    if (!error) {
      body = (
        <p>
          Hi {username}! Welcome! Did you know that it's {manilaTime} in Manila
          and {melbourneTime} in Melbourne right now?
        </p>
      );
    } else {
      body = (
        <p>
          Sorry {username}, {error}
        </p>
      );
    }
  }

  return (
    <Layout>
      <>
        <img src={logo} className="App-logo" alt="logo" />
        {body}
      </>
    </Layout>
  );
}
