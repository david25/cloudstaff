export default async function getJsonData(endpoint: string) {
  return fetch(endpoint, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Request-Headers": "*",
      "Access-Control-Allow-Headers": "*",
    },
  }).then((response) => response.json());
}
