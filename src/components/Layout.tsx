import React from "react";

export default function Layout({ children }: { children: JSX.Element }) {
  return (
    <div className="App">
      <header className="App-header">{children}</header>
    </div>
  );
}
